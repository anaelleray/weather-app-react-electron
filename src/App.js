import './App.css';
import React from "react";
//import ReactDOM from "react-dom";
import WeatherComponent from './Component/WeatherComponent';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <WeatherComponent></WeatherComponent>
      </header>
    </div>
  );
}

export default App;
